package com.dyh.sv_learn;

import android.media.AudioRecord;

/**
 * describe: Android 音视频开发(二)：使用 AudioRecord 采集音频PCM并保存到文件
 * create by daiyh on 2021-9-26
 */
public class Two {

//AudioRecord API
/**
 * AudioRecord是Android系统提供的用于实现录音的功能类。
 * AndioRecord类的主要功能是让各种JAVA应用能够管理音频资源，以便它们通过此类能够录制声音相关的硬件所收集的声音。
 * 此功能的实现就是通过”pulling”（读取）AudioRecord对象的声音数据来完成的。
 * 在录音过程中，应用所需要做的就是通过后面三个类方法中的一个去及时地获取AudioRecord对象的录音数据
 * AudioRecord类提供的三个获取声音数据的方法分别是
 * read(byte[], int, int), read(short[], int, int), read(ByteBuffer, int)
 * 无论选择使用那一个方法都必须事先设定方便用户的声音数据的存储格式。
 * 开始录音的时候，AudioRecord需要初始化一个相关联的声音buffer, 这个buffer主要是用来保存新的声音数据。
 * 这个buffer的大小，我们可以在对象构造期间去指定。
 * 它表明一个AudioRecord对象还没有被读取（同步）声音数据前能录多长的音(即一次可以录制的声音容量)。
 * 声音数据从音频硬件中被读出，数据大小不超过整个录音数据的大小（可以分多次读出），即每次读取初始化buffer容量的数据。
 * <p>
 * 实现Android录音的流程为：
 * 1.构造一个AudioRecord对象，其中需要的最小录音缓存buffer大小可以通过getMinBufferSize方法得到。
 * 如果buffer容量过小，将导致对象构造的失败。
 * 2.初始化一个buffer，该buffer大于等于AudioRecord对象用于写声音数据的buffer大小。
 * 3.开始录音
 * 4.创建一个数据流，一边从AudioRecord中读取声音数据到初始化的buffer，一边将buffer中数据导入数据流。
 * 5.关闭数据流
 * 6.停止录音
 * <p>
 * //首先要声明一些全局的变量参数：
 * private AudioRecord audioRecord = null;  // 声明 AudioRecord 对象
 * private int recordBufSize = 0; // 声明recoordBufffer的大小字段
 * //获取buffer的大小并创建AudioRecord：
 * public void createAudioRecord() {
 * recordBufSize = AudioRecord.getMinBufferSize(frequency, channelConfiguration, EncodingBitRate);  //audioRecord能接受的最小的buffer大小
 * audioRecord = new AudioRecord(MediaRecorder.AudioSource.MIC, frequency, channelConfiguration, EncodingBitRate, recordBufSize);
 * }
 * //初始化一个buffer
 * byte data[] = new byte[recordBufSize];
 * //开始录音
 * audioRecord.startRecording();
 * isRecording = true;
 * //创建一个数据流，一边从AudioRecord中读取声音数据到初始化的buffer，一边将buffer中数据导入数据流
 * FileOutputStream os = null;
 * try {
 * os = new FileOutputStream(filename);
 * } catch (FileNotFoundException e) {
 * e.printStackTrace();
 * }
 * <p>
 * //首先要声明一些全局的变量参数：
 * private AudioRecord audioRecord = null;  // 声明 AudioRecord 对象
 * private int recordBufSize = 0; // 声明recoordBufffer的大小字段
 * //获取buffer的大小并创建AudioRecord：
 * public void createAudioRecord() {
 * recordBufSize = AudioRecord.getMinBufferSize(frequency, channelConfiguration, EncodingBitRate);  //audioRecord能接受的最小的buffer大小
 * audioRecord = new AudioRecord(MediaRecorder.AudioSource.MIC, frequency, channelConfiguration, EncodingBitRate, recordBufSize);
 * }
 * //初始化一个buffer
 * byte data[] = new byte[recordBufSize];
 * //开始录音
 * audioRecord.startRecording();
 * isRecording = true;
 * //创建一个数据流，一边从AudioRecord中读取声音数据到初始化的buffer，一边将buffer中数据导入数据流
 * FileOutputStream os = null;
 * try {
 * os = new FileOutputStream(filename);
 * } catch (FileNotFoundException e) {
 * e.printStackTrace();
 * }
 * <p>
 * //首先要声明一些全局的变量参数：
 * private AudioRecord audioRecord = null;  // 声明 AudioRecord 对象
 * private int recordBufSize = 0; // 声明recoordBufffer的大小字段
 * //获取buffer的大小并创建AudioRecord：
 * public void createAudioRecord() {
 * recordBufSize = AudioRecord.getMinBufferSize(frequency, channelConfiguration, EncodingBitRate);  //audioRecord能接受的最小的buffer大小
 * audioRecord = new AudioRecord(MediaRecorder.AudioSource.MIC, frequency, channelConfiguration, EncodingBitRate, recordBufSize);
 * }
 * //初始化一个buffer
 * byte data[] = new byte[recordBufSize];
 * //开始录音
 * audioRecord.startRecording();
 * isRecording = true;
 * //创建一个数据流，一边从AudioRecord中读取声音数据到初始化的buffer，一边将buffer中数据导入数据流
 * FileOutputStream os = null;
 * try {
 * os = new FileOutputStream(filename);
 * } catch (FileNotFoundException e) {
 * e.printStackTrace();
 * }
 * <p>
 * //首先要声明一些全局的变量参数：
 * private AudioRecord audioRecord = null;  // 声明 AudioRecord 对象
 * private int recordBufSize = 0; // 声明recoordBufffer的大小字段
 * //获取buffer的大小并创建AudioRecord：
 * public void createAudioRecord() {
 * recordBufSize = AudioRecord.getMinBufferSize(frequency, channelConfiguration, EncodingBitRate);  //audioRecord能接受的最小的buffer大小
 * audioRecord = new AudioRecord(MediaRecorder.AudioSource.MIC, frequency, channelConfiguration, EncodingBitRate, recordBufSize);
 * }
 * //初始化一个buffer
 * byte data[] = new byte[recordBufSize];
 * //开始录音
 * audioRecord.startRecording();
 * isRecording = true;
 * //创建一个数据流，一边从AudioRecord中读取声音数据到初始化的buffer，一边将buffer中数据导入数据流
 * FileOutputStream os = null;
 * try {
 * os = new FileOutputStream(filename);
 * } catch (FileNotFoundException e) {
 * e.printStackTrace();
 * }
 * <p>
 * //首先要声明一些全局的变量参数：
 * private AudioRecord audioRecord = null;  // 声明 AudioRecord 对象
 * private int recordBufSize = 0; // 声明recoordBufffer的大小字段
 * //获取buffer的大小并创建AudioRecord：
 * public void createAudioRecord() {
 * recordBufSize = AudioRecord.getMinBufferSize(frequency, channelConfiguration, EncodingBitRate);  //audioRecord能接受的最小的buffer大小
 * audioRecord = new AudioRecord(MediaRecorder.AudioSource.MIC, frequency, channelConfiguration, EncodingBitRate, recordBufSize);
 * }
 * //初始化一个buffer
 * byte data[] = new byte[recordBufSize];
 * //开始录音
 * audioRecord.startRecording();
 * isRecording = true;
 * //创建一个数据流，一边从AudioRecord中读取声音数据到初始化的buffer，一边将buffer中数据导入数据流
 * FileOutputStream os = null;
 * try {
 * os = new FileOutputStream(filename);
 * } catch (FileNotFoundException e) {
 * e.printStackTrace();
 * }
 * <p>
 * //首先要声明一些全局的变量参数：
 * private AudioRecord audioRecord = null;  // 声明 AudioRecord 对象
 * private int recordBufSize = 0; // 声明recoordBufffer的大小字段
 * //获取buffer的大小并创建AudioRecord：
 * public void createAudioRecord() {
 * recordBufSize = AudioRecord.getMinBufferSize(frequency, channelConfiguration, EncodingBitRate);  //audioRecord能接受的最小的buffer大小
 * audioRecord = new AudioRecord(MediaRecorder.AudioSource.MIC, frequency, channelConfiguration, EncodingBitRate, recordBufSize);
 * }
 * //初始化一个buffer
 * byte data[] = new byte[recordBufSize];
 * //开始录音
 * audioRecord.startRecording();
 * isRecording = true;
 * //创建一个数据流，一边从AudioRecord中读取声音数据到初始化的buffer，一边将buffer中数据导入数据流
 * FileOutputStream os = null;
 * try {
 * os = new FileOutputStream(filename);
 * } catch (FileNotFoundException e) {
 * e.printStackTrace();
 * }
 */

//使用 AudioRecord 实现录音，并生成wav
/**
 *  //首先要声明一些全局的变量参数：
 *     private AudioRecord audioRecord = null;  // 声明 AudioRecord 对象
 *     private int recordBufSize = 0; // 声明recoordBufffer的大小字段
 * //获取buffer的大小并创建AudioRecord：
 *     public void createAudioRecord() {
 *         recordBufSize = AudioRecord.getMinBufferSize(frequency, channelConfiguration, EncodingBitRate);  //audioRecord能接受的最小的buffer大小
 *         audioRecord = new AudioRecord(MediaRecorder.AudioSource.MIC, frequency, channelConfiguration, EncodingBitRate, recordBufSize);
 *     }
 * //初始化一个buffer
 *     byte data[] = new byte[recordBufSize];
 * //开始录音
 *     audioRecord.startRecording();
 *     isRecording = true;
 * //创建一个数据流，一边从AudioRecord中读取声音数据到初始化的buffer，一边将buffer中数据导入数据流
 *     FileOutputStream os = null;
 *     try {
 *         os = new FileOutputStream(filename);
 *     } catch (FileNotFoundException e) {
 *         e.printStackTrace();
 *     }
 *     if (null != os) {
 *         while (isRecording) {
 *             read = audioRecord.read(data, 0, recordBufSize);
 * 　　　　    // 如果读取音频数据没有出现错误，就将数据写入到文件
 *            if (AudioRecord.ERROR_INVALID_OPERATION != read) {
 *                try {
 *                    os.write(data);
 *                } catch (IOException e) {
 *                    e.printStackTrace();
 *                }
 *            }
 *         }
 *
 *         try {
 *             os.close();
 *         } catch (IOException e) {
 *             e.printStackTrace();
 *         }
 *     }
 * //关闭数据流
 *  //修改标志位：isRecording 为false，上面的while循环就自动停止了，数据流也就停止流动了，Stream也就被关闭了。
 *     isRecording = false;
 * //停止录音
 *  //停止录音之后，注意要释放资源
 *     if (null != audioRecord) {
 * 　　    audioRecord.stop();
 *         audioRecord.release();
 * 　　    audioRecord = null;
 *         recordingThread = null;
 *     }
 *
 * //在文件的数据开头加入WAVE HEAD数据即可，也就是文件头。
 *   只有加上文件头部的数据，播放器才能正确的知道里面的内容到底是什么，进而能够正常的解析并播放里面的内容
 *   添加WAVE文件头的代码如下：
 *   public class PcmToWavUtil {
 *       //缓存的音频大小
 *       private int mBufferSize;
 *       //采样率
 *       private int mSampleRate;
 *       //声道数
 *       private int mChannel;
 *  
 *       //encoding是音频格式
 *       PcmToWavUtil(int sampleRate, int channel, int encoding) {
 *           this.mSampleRate = sampleRate.;
 *           this.mChannel = channel;
 *           this.mBufferSize = AudioRecord.getMinBufferSize(mSampleRate, mChannel, encoding);
 *       }
 *
 *   }
 *
 */

}

