package com.dyh.sv_learn;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.widget.ImageView;

import java.io.File;

/**
 * describe:  Android 音视频开发(一) : 通过三种方式绘制图片
 * create by daiyh on 2021-9-24
 */
public class One {
    //Android 音视频开发(一) : 通过三种方式绘制图片
    //--------------------------------------------------------------
    //在 Android 平台绘制一张图片，使用至少 3 种不同的 API，ImageView，SurfaceView，自定义 View
    //加载内存中的图片需要增加权限：<uses-permission android:name="android.permission.READ_EXTERNAL_STORAGE"/>
    //1.ImageView 绘制图片
    /**
     * Bitmap bitmap = BitmapFactory.decodeFile(
     *              Environment.getExternalStorageDirectory().getPath() + File.separator + "11.jpg");
     * imageView.setImageBitmap(bitmap);
     */

    //2.SurfaceView 绘制图片
    /**
     * SurfaceView surfaceView = (SurfaceView) findViewById(R.id.surface);
     * surfaceView.getHolder().addCallback(new SurfaceHolder.Callback() {
     *     @Override
     *     public void surfaceCreated(SurfaceHolder surfaceHolder) {
     *
     *         if (surfaceHolder == null) {
     *             return;
     *         }
     *
     *         Paint paint = new Paint();
     *         paint.setAntiAlias(true);
     *         paint.setStyle(Paint.Style.STROKE);
     *
     *         Bitmap bitmap = BitmapFactory.decodeFile(Environment.getExternalStorageDirectory().getPath() + File.separator + "11.jpg");  // 获取bitmap
     *         Canvas canvas = surfaceHolder.lockCanvas();  // 先锁定当前surfaceView的画布
     *         canvas.drawBitmap(bitmap, 0, 0, paint); //执行绘制操作
     *         surfaceHolder.unlockCanvasAndPost(canvas); // 解除锁定并显示在界面上
     *     }
     *
     *     @Override
     *     public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i1, int i2) {
     *
     *     }
     *
     *     @Override
     *     public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
     *
     *     }
     * });
     */

    //3.自定义 View 绘制图片
    /**
     * public class CustomView extends View {
     *     Paint paint = new Paint();
     *     Bitmap bitmap;
     *     public CustomView(Context context) {
     *        super(context);
     *        paint.setAntiAlias(true);
     *        paint.setStyle(Paint.Style.STROKE);
     *        bitmap = BitmapFactory.decodeFile(Environment.getExternalStorageDirectory().getPath() + File.separator + "11.jpg");  // 获取bitmap
     *     }
     *    @Override
     *    protected void onDraw(Canvas canvas) {
     *        super.onDraw(canvas);
     *        // 不建议在onDraw做任何分配内存的操作
     *        if (bitmap != null) {
     *            canvas.drawBitmap(bitmap, 0, 0, paint);
     *        }
     *    }
     * }
     */

    //--------------------------------------------------------------
}
