package com.dyh.note1;

import android.app.Application;

import com.alibaba.android.arouter.launcher.ARouter;

/**
 * describe:
 * create by daiyh on 2021-7-19
 */
public class MyApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        ARouter.init(this);
    }
}
