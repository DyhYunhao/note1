package com.dyh.face;

import android.app.Application;
import android.util.Log;

import com.squareup.leakcanary.LeakCanary;

/**
 * describe:
 * create by daiyh on 2021-7-12
 */
public class BaseApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        if (LeakCanary.isInAnalyzerProcess(this)) {
            Log.d("00000", "初始化");
            return;
        }
        LeakCanary.install(this);
    }
}
