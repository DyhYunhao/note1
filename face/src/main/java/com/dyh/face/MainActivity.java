package com.dyh.face;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.media.Image;
import android.os.Bundle;
import android.print.PageRange;
import android.text.TextUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.dyh.face.arcsoft.ArcsoftTools;
import com.dyh.face.arcsoft.callback.ISpotCallback;
import com.dyh.face.arcsoft.utils.face.FaceHelper;
import com.squareup.haha.perflib.Main;

import java.io.File;

public class MainActivity extends AppCompatActivity {

    private Button mBtnStart, mBtnRegister, mBtnDelete, mBtnUnRegister;
    private EditText mEtFaceId;
    private TextView mTvFaceId;
    private ImageView mIvHead;

    private ArcsoftTools tools = null;

    /**
     * 所需权限列表
     */
    private static final String[] PERMISSIONS = new String[] {
            Manifest.permission.CAMERA,
            Manifest.permission.READ_PHONE_STATE
    };
    private static final int ACTION_REQUEST_PERMISSIONS = 0x001;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();

        //激活SDK
        ArcsoftTools.activateSDK(this);

        if (!checkPermissions(PERMISSIONS)) {
            ActivityCompat.requestPermissions(this, PERMISSIONS, ACTION_REQUEST_PERMISSIONS);
        } else {
            if(tools == null) {
                tools = new ArcsoftTools(this, new SpotCallback());
            }
            tools.startRecognize();
        }

        initEvent();
    }

    private void initView() {
        mBtnStart = findViewById(R.id.btn_start);
        mBtnRegister = findViewById(R.id.btn_register);
        mBtnDelete = findViewById(R.id.btn_delete);
        mBtnUnRegister = findViewById(R.id.btn_unRegister);
        mEtFaceId = findViewById(R.id.et_face_id);
        mTvFaceId = findViewById(R.id.tv_show_faceId);
        mIvHead = findViewById(R.id.iv_head);
    }

    @SuppressLint("SetTextI18n")
    private void initEvent() {
        mBtnStart.setOnClickListener(v -> {
            mTvFaceId.setText("开始识别……");
            mIvHead.setImageDrawable(null);
            if(tools == null) {
                tools = new ArcsoftTools(this, new SpotCallback());
            }
            tools.startRecognize();
        });

        mBtnRegister.setOnClickListener(v -> {
            mTvFaceId.setText("注册中……");
            tools.register(MainActivity.this, faceId -> {
                mTvFaceId.setText("注册的ID：" + faceId);
            });
        });

        mBtnDelete.setOnClickListener(v -> {
            final String face_id = mEtFaceId.getText().toString();
            if (!TextUtils.isEmpty(face_id)) {
                AlertDialog dialog = new AlertDialog.Builder(MainActivity.this)
                        .setTitle("notification")
                        .setMessage("delete those " + " images and face features?")
                        .setPositiveButton("OK", (dialog1, which) -> {
                            mTvFaceId.setText("删除注册人脸数据：" + face_id);
                            tools.deleteRegisterFaceId(face_id);
                        }).setNegativeButton("取消", null).create();
                dialog.show();
            } else {
                showToast("请输入用户face Id");
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        boolean isAllGranted = true;
        for (int grantResult : grantResults) {
            isAllGranted &= (grantResult == PackageManager.PERMISSION_GRANTED);
        }
        afterRequestPermission(requestCode, isAllGranted);
    }

    private void afterRequestPermission(int requestCode, boolean isAllGranted) {
        if (requestCode == ACTION_REQUEST_PERMISSIONS) {
            if (isAllGranted) {
                if(tools == null) {
                    tools = new ArcsoftTools(this, new SpotCallback());
                }
                tools.startRecognize();
            } else {
                showToast("permission denied!");
            }
        }
    }

    /**
     * 权限检查
     *
     * @param neededPermissions 需要的权限
     * @return 是否全部被允许
     */
    protected boolean checkPermissions(String[] neededPermissions) {
        if (neededPermissions == null || neededPermissions.length == 0) {
            return true;
        }
        boolean allGranted = true;
        for (String neededPermission : neededPermissions) {
            allGranted &= ContextCompat.checkSelfPermission(this, neededPermission) == PackageManager.PERMISSION_GRANTED;
        }
        return allGranted;
    }

    /**
     * 识别结果的监听
     */
    private class SpotCallback implements ISpotCallback {
        @Override
        public void detectFail() {
            mTvFaceId.setText("未检测到人脸！");
        }

        @Override
        public void unRegister(Bitmap bitmap) {
            mTvFaceId.setText("人脸未录入，请先录入~");
            mIvHead.setImageBitmap(bitmap);
        }

        @Override
        public void spotFaceId(String faceId) {
            String msg = "识别成功：" +faceId;
            mTvFaceId.setText(msg);
            File headFile = FaceHelper.getHeadFile(faceId);
            Glide.with(MainActivity.this)
                    .load(headFile)
                    .into(mIvHead);
        }
    }

    protected void showToast(String s) {
        Toast.makeText(getApplicationContext(), s, Toast.LENGTH_SHORT).show();
    }
}