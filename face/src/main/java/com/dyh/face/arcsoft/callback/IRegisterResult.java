package com.dyh.face.arcsoft.callback;

/**
 * describe:
 * create by daiyh on 2021-7-15
 */
public interface IRegisterResult {
    void completed(String faceId);
}
