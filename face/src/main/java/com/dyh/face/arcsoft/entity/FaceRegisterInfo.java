package com.dyh.face.arcsoft.entity;

/**
 * describe: 人脸注册结果实体类
 * create by daiyh on 2021-7-13
 */
public class FaceRegisterInfo {
    private byte[] featureData;
    private String name;

    public FaceRegisterInfo(byte[] featureData, String name) {
        this.featureData = featureData;
        this.name = name;
    }

    public byte[] getFeatureData() {
        return featureData;
    }

    public void setFeatureData(byte[] featureData) {
        this.featureData = featureData;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
