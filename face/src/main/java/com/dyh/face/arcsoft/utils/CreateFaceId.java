package com.dyh.face.arcsoft.utils;

/**
 * describe: 生成faceId
 * create by daiyh on 2021-7-14
 */
public class CreateFaceId {
    /**
     * 生成faceId的方法
     * */
    public static String getFaceId(){
        String sn = getDeviceSN();
        String username = sn + System.currentTimeMillis();
        return username;
    }

    private static String getDeviceSN(){

        String serialNumber = android.os.Build.SERIAL;

        return serialNumber;
    }
}
