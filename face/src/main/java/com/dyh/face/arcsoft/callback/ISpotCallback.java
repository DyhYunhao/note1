package com.dyh.face.arcsoft.callback;

import android.graphics.Bitmap;

/**
 * describe: 识别回调接口
 * create by daiyh on 2021-7-13
 */
public interface ISpotCallback {
    /**
     * 未检测到人脸
     */
    void detectFail();

    /**
     * 人脸未注册
     */
    void unRegister(Bitmap bitmap);

    /**
     * 识别成功
     * @param faceId
     */
    void spotFaceId(String faceId);
}
