package com.dyh.face.arcsoft.common;

import android.hardware.Camera;

import java.security.PublicKey;

/**
 * describe: 常量类
 * create by daiyh on 2021-7-13
 */
public class Constants {

    /**
     * 虹软识别demo的APPID和SDKKEY
     */
    public static final String APP_ID = "Ctx24TF4sGWooZK9iVwr8UcQh854TLx9c8KXVb5jip3b";
    public static final String SDK_KEY = "GjGCXY57m4M5DkNN8YH92GhFi8VDWmvtGWyLrpzkykat";

    /**
     * IR预览数据相对于RGB预览数据的横向偏移量，注意：是预览数据，一般的摄像头的预览数据都是 width > height
     */
    public static final int HORIZONTAL_OFFSET = 0;
    /**
     * IR预览数据相对于RGB预览数据的纵向偏移量，注意：是预览数据，一般的摄像头的预览数据都是 width > height
     */
    public static final int VERTICAL_OFFSET = 0;
    public static final int MAX_DETECT_NUM = 10;

    public static final int MAX_DETECT_FAIL_NUM = 120;

    /**
     * 活体检测的开关
     */
    public static boolean livenessDetect = true;
    /**
     * 当FR成功，活体未成功时，FR等待活体的时间
     */
    public static final int WAIT_LIVENESS_INTERVAL = 100;
    /**
     * 失败重试间隔时间（ms）
     */
    public static final long FAIL_RETRY_INTERVAL = 1000;
    /**
     * 出错重试最大次数
     */
    public static final int MAX_RETRY_TIME = 3;
    /**
     * 优先打开的摄像头，本界面主要用于单目RGB摄像头设备，因此默认打开前置
     */
    public static Integer RGB_CAMERA_ID = Camera.CameraInfo.CAMERA_FACING_FRONT;
//    public static Integer RGB_CAMERA_ID = Camera.CameraInfo.CAMERA_FACING_BACK;
    /**
     * 识别阈值
     */
    public static final float SIMILAR_THRESHOLD = 0.8F;


}
