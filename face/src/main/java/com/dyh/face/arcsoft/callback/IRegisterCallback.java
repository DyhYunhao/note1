package com.dyh.face.arcsoft.callback;

/**
 * describe: 注册完成回调
 * create by daiyh on 2021-7-13
 */
public interface IRegisterCallback {
    void registerResult(String faceId);
}
