package com.dyh.face.arcsoft;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.graphics.Rect;
import android.hardware.Camera;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.Toast;

import com.arcsoft.face.ErrorInfo;
import com.arcsoft.face.FaceEngine;
import com.arcsoft.face.FaceFeature;
import com.arcsoft.face.FaceInfo;
import com.arcsoft.face.LivenessInfo;
import com.arcsoft.face.VersionInfo;
import com.arcsoft.face.enums.DetectFaceOrientPriority;
import com.arcsoft.face.enums.DetectMode;
import com.arcsoft.imageutil.ArcSoftImageFormat;
import com.dyh.face.R;
import com.dyh.face.arcsoft.callback.IRegisterCallback;
import com.dyh.face.arcsoft.callback.IRegisterResult;
import com.dyh.face.arcsoft.callback.ISpotCallback;
import com.dyh.face.arcsoft.common.Constants;
import com.dyh.face.arcsoft.entity.FacePreviewInfo;
import com.dyh.face.arcsoft.faceserver.CompareResult;
import com.dyh.face.arcsoft.faceserver.FaceTools;
import com.dyh.face.arcsoft.utils.ConfigUtil;
import com.dyh.face.arcsoft.utils.ImageUtil;
import com.dyh.face.arcsoft.utils.ToastUtil;
import com.dyh.face.arcsoft.utils.camera.CameraHelper;
import com.dyh.face.arcsoft.utils.camera.CameraListener;
import com.dyh.face.arcsoft.utils.face.FaceHelper;
import com.dyh.face.arcsoft.utils.face.FaceListener;
import com.dyh.face.arcsoft.utils.face.LivenessType;
import com.dyh.face.arcsoft.utils.face.RequestFeatureStatus;
import com.dyh.face.arcsoft.utils.face.RequestLivenessStatus;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.Map;
import java.util.PropertyResourceBundle;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.Nullable;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

import static android.content.Context.WINDOW_SERVICE;
import static com.dyh.face.arcsoft.common.Constants.FAIL_RETRY_INTERVAL;
import static com.dyh.face.arcsoft.common.Constants.MAX_DETECT_FAIL_NUM;
import static com.dyh.face.arcsoft.common.Constants.MAX_DETECT_NUM;
import static com.dyh.face.arcsoft.common.Constants.MAX_RETRY_TIME;
import static com.dyh.face.arcsoft.common.Constants.RGB_CAMERA_ID;
import static com.dyh.face.arcsoft.common.Constants.SIMILAR_THRESHOLD;
import static com.dyh.face.arcsoft.common.Constants.WAIT_LIVENESS_INTERVAL;

/**
 * describe: 虹软人脸识别工具类
 * create by daiyh on 2021-7-12
 */
public class ArcsoftTools {
    private static final String TAG = "dyh";
    private Context context;
    private ISpotCallback mSpotCallback;

    //video模式人脸检测引擎，用于预览帧人脸追踪
    private FaceEngine ftEngine;
    //用于特征提取的引擎
    private FaceEngine frEngine;
    //IMAGE模式活体检测引擎，用于预览帧人脸活体检测
    private FaceEngine flEngine;

    private int ftInitCode = -1;
    private int frInitCode = -1;
    private int flInitCode = -1;

    private CameraHelper cameraHelper;
    private FaceHelper faceHelper;
    private List<CompareResult> compareResultList;

    private Camera.Size previewSize;

    //相机预览显示的控件，可为SurfaceView或TextureView
    private View previewView;
    //注册人脸状态码，准备注册
    private static final int REGISTER_STATUS_READY = 0;
    //注册人脸状态码，注册中
    private static final int REGISTER_STATUS_DONE = 2;

    private int registerStatus = REGISTER_STATUS_READY;
    //用于记录人脸识别相关状态
    private ConcurrentHashMap<Integer, Integer> requestFeatureStatusMap = new ConcurrentHashMap<>();
    //用于记录人脸特征提取出错重试次数
    private ConcurrentHashMap<Integer, Integer> extractErrorRetryMap = new ConcurrentHashMap<>();
    //用于存储活体值
    private ConcurrentHashMap<Integer, Integer> livenessMap = new ConcurrentHashMap<>();
    //用于存储活体检测出错重试次数
    private ConcurrentHashMap<Integer, Integer> livenessErrorRetryMap = new ConcurrentHashMap<>();

    private CompositeDisposable getFeatureDelayedDisposables = new CompositeDisposable();
    private CompositeDisposable delayFaceTaskCompositeDisposable = new CompositeDisposable();

    private WindowManager mWindowManager = null;
    private View mView;
    public static boolean isActivateComplete = false;
    //注册相关数据保存
    private byte[] mFaceData = null;
    private List<FacePreviewInfo> mFacePreviewInfoList = null;
    private int detect_fail_num = 0;

    public ArcsoftTools(Context context, ISpotCallback mSpotCallback) {
        compareResultList = new ArrayList<>();
        this.context = context;
        this.mSpotCallback = mSpotCallback;
    }

    ////////////////////////////////////////////////////////////////////////////
    /**
    * 初始化
    */
    public static void activateSDK(final Context context) {
        new Thread(() -> {
            int activatedNum = 0;
            while (true) {
                //激活
                int activeCode = FaceEngine.activeOnline(context, Constants.APP_ID, Constants.SDK_KEY);
                if (activeCode == ErrorInfo.MOK) {
                    isActivateComplete = true;
                    ToastUtil.show(context, "activate success");
                    break;
                } else if (activeCode == ErrorInfo.MERR_ASF_ALREADY_ACTIVATED) {
                    isActivateComplete = true;
                    ToastUtil.show(context, "already activated");
                    break;
                } else { // 失败
                    if (activatedNum > 10) { // max retry activate 10
                        ToastUtil.show(context, "activate failed");
                        break;
                    } else {
                        Log.e(TAG, "activate will retry");
                        activatedNum += 1;
                        try {
                            Thread.sleep(1000);
                        } catch (InterruptedException e) {
                            Toast.makeText(context, e.getMessage(), Toast.LENGTH_SHORT).show();
                            e.printStackTrace();
                        }
                    }
                }
            }
        }).start();
    }

    /**
     * 开始人脸识别
     */
    public void startRecognize() {
        if(isActivateComplete){
            mFaceData = null;
            mFacePreviewInfoList = null;
            detect_fail_num = 0;

            //本地人脸库初始化
            FaceTools.getInstance().init(context);

            createCameraPreview();
            initEngine();
            initCamera();
        }else {
            ToastUtil.show(context, "Incomplete activation");
        }
    }

    /**
     * 注册人脸
     */
    public void register(Context context, final IRegisterCallback callback) {
        //人脸检测成功的情况下，才能注册
        if (mFaceData != null && mFacePreviewInfoList != null) {
            FaceTools.getInstance().init(context);
            registerFace(mFaceData, mFacePreviewInfoList, new IRegisterResult() {
                @Override
                public void completed(String faceId) {
                    mFaceData = null;
                    mFacePreviewInfoList = null;
                    FaceTools.getInstance().unInit();
                    if(callback != null){
                        callback.registerResult(faceId);
                    }
                }
            });
        }else {
            showToast("no face data");
            if(callback != null){
                callback.registerResult(null);
            }
        }
    }

    /**
     * 清空人脸识别结果
     * */
    public void deleteAllRegisterFace(){
        int faceNum = FaceTools.getInstance().getFaceNumber(context);
        if (faceNum == 0) {
            showToast("no face need to delete");
        } else {
            int deleteCount = FaceTools.getInstance().clearAllFace(context);
            showToast(deleteCount + " faces cleared!");
        }
    }

    /**
     * 删除某个用户的face Id
     * */
    public boolean deleteRegisterFaceId(final String faceId){
        if(TextUtils.isEmpty(faceId)){
            showToast("请输入用户 face Id");
            return false;
        }else {

            int faceNum = FaceTools.getInstance().getFaceNumber(context);
            if (faceNum == 0) {
                showToast("no face need to delete");
                return false;
            } else {
                boolean isSuccess = FaceTools.getInstance().clearFaceId(context, faceId);
                showToast("删除用户 face id: " + faceId + ", 删除结果：" + isSuccess);
                return isSuccess;
            }
        }

    }



    private void createCameraPreview(){
        mView = View.inflate(context, R.layout.preview_camera, null);
        previewView = mView.findViewById(R.id.single_camera_texture_preview);
        mWindowManager = (WindowManager) context.getSystemService(WINDOW_SERVICE);
        WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
        layoutParams.width = 400;
        layoutParams.height = 600;
        layoutParams.gravity = Gravity.TOP;
        mWindowManager.addView(mView, layoutParams);
    }

    /**
     * 初始化引擎
     */
    private void initEngine() {
        ftEngine = new FaceEngine();
        ftInitCode = ftEngine.init(context, DetectMode.ASF_DETECT_MODE_VIDEO, ConfigUtil.getFtOrient(context),
                16, MAX_DETECT_NUM, FaceEngine.ASF_FACE_DETECT);

        frEngine = new FaceEngine();
        frInitCode = frEngine.init(context, DetectMode.ASF_DETECT_MODE_IMAGE, DetectFaceOrientPriority.ASF_OP_0_ONLY,
                16, MAX_DETECT_NUM, FaceEngine.ASF_FACE_RECOGNITION);

        flEngine = new FaceEngine();
        flInitCode = flEngine.init(context, DetectMode.ASF_DETECT_MODE_IMAGE, DetectFaceOrientPriority.ASF_OP_0_ONLY,
                16, MAX_DETECT_NUM, FaceEngine.ASF_LIVENESS);


        VersionInfo versionInfo = new VersionInfo();
        ftEngine.getVersion(versionInfo);
        Log.i(TAG, "initEngine:  init: " + ftInitCode + "  version:" + versionInfo);

        if (ftInitCode != ErrorInfo.MOK) {
            Log.e(TAG, "initEngine error code: " + ftInitCode);
        }
        if (frInitCode != ErrorInfo.MOK) {
            Log.e(TAG, "initEngine error code: " + frInitCode);
        }
        if (flInitCode != ErrorInfo.MOK) {
            Log.e(TAG, "initEngine error code: " + flInitCode);
        }

    }

    private void initCamera() {


        final FaceListener faceListener = new FaceListener() {
            @Override
            public void onFail(Exception e) {
                Log.e(TAG, "onFail: " + e.getMessage());
            }

            //请求FR的回调
            @Override
            public void onFaceFeatureInfoGet(@Nullable final FaceFeature faceFeature, final Integer requestId, final Integer errorCode) {
                //FR成功
                if (faceFeature != null) {
//                    Log.i(TAG, "onPreview: fr end = " + System.currentTimeMillis() + " trackId = " + requestId);
                    Integer liveness = livenessMap.get(requestId);
                    //不做活体检测的情况，直接搜索
                    if (!Constants.livenessDetect) {
                        searchFace(faceFeature, requestId);
                    }
                    //活体检测通过，搜索特征
                    else if (liveness != null && liveness == LivenessInfo.ALIVE) {
                        searchFace(faceFeature, requestId);
                    }
                    //活体检测未出结果，或者非活体，延迟执行该函数
                    else {
                        if (requestFeatureStatusMap.containsKey(requestId)) {
                            Observable.timer(WAIT_LIVENESS_INTERVAL, TimeUnit.MILLISECONDS)
                                    .subscribe(new Observer<Long>() {
                                        Disposable disposable;

                                        @Override
                                        public void onSubscribe(Disposable d) {
                                            disposable = d;
                                            getFeatureDelayedDisposables.add(disposable);
                                        }

                                        @Override
                                        public void onNext(Long aLong) {
                                            onFaceFeatureInfoGet(faceFeature, requestId, errorCode);
                                        }

                                        @Override
                                        public void onError(Throwable e) {

                                        }

                                        @Override
                                        public void onComplete() {
                                            getFeatureDelayedDisposables.remove(disposable);
                                        }
                                    });
                        }
                    }

                }else { //特征提取失败

                    if (increaseAndGetValue(extractErrorRetryMap, requestId) > MAX_RETRY_TIME) {

                        extractErrorRetryMap.put(requestId, 0);

                        String msg;
                        // 传入的FaceInfo在指定的图像上无法解析人脸，此处使用的是RGB人脸数据，一般是人脸模糊
                        if (errorCode != null && errorCode == ErrorInfo.MERR_FSDK_FACEFEATURE_LOW_CONFIDENCE_LEVEL) {
                            msg = "face low confidence level";
                        } else {
                            msg = "ExtractCode:" + errorCode;
                        }
                        faceHelper.setName(requestId, "DENIED: " + msg);
                        // 在尝试最大次数后，特征提取仍然失败，则认为识别未通过
                        requestFeatureStatusMap.put(requestId, RequestFeatureStatus.FAILED);

//                        retryRecognizeDelayed(requestId);


                        // 检测人脸失败
                        if(mSpotCallback != null){
                            mSpotCallback.detectFail();
                        }
                        stopRecognize();

                    } else {
                        requestFeatureStatusMap.put(requestId, RequestFeatureStatus.TO_RETRY);
                    }
                }
            }

            @Override
            public void onFaceLivenessInfoGet(@Nullable LivenessInfo livenessInfo, final Integer requestId, Integer errorCode) {
                if (livenessInfo != null) {
                    int liveness = livenessInfo.getLiveness();
                    livenessMap.put(requestId, liveness);
                    // 非活体，重试
                    if (liveness == LivenessInfo.NOT_ALIVE) {
                        faceHelper.setName(requestId, "" + "NOT_ALIVE");
                        // 延迟 FAIL_RETRY_INTERVAL 后，将该人脸状态置为UNKNOWN，帧回调处理时会重新进行活体检测
                        retryLivenessDetectDelayed(requestId);
                    }
                } else {
                    if (increaseAndGetValue(livenessErrorRetryMap, requestId) > MAX_RETRY_TIME) {
                        livenessErrorRetryMap.put(requestId, 0);
                        String msg;
                        // 传入的FaceInfo在指定的图像上无法解析人脸，此处使用的是RGB人脸数据，一般是人脸模糊
                        if (errorCode != null && errorCode == ErrorInfo.MERR_FSDK_FACEFEATURE_LOW_CONFIDENCE_LEVEL) {
                            msg = "face low confidence level";
                        } else {
                            msg = "ProcessCode:" + errorCode;
                        }
                        faceHelper.setName(requestId, msg);
                        retryLivenessDetectDelayed(requestId);
                    } else {
                        livenessMap.put(requestId, LivenessInfo.UNKNOWN);
                    }
                }
            }


        };


        CameraListener cameraListener = new CameraListener() {
            @Override
            public void onCameraOpened(Camera camera, int cameraId, int displayOrientation, boolean isMirror) {
                Camera.Size lastPreviewSize = previewSize;
                previewSize = camera.getParameters().getPreviewSize();


                Log.i(TAG, "onCameraOpened: ");
                // 切换相机的时候可能会导致预览尺寸发生变化
                if (faceHelper == null ||
                        lastPreviewSize == null ||
                        lastPreviewSize.width != previewSize.width || lastPreviewSize.height != previewSize.height) {
                    Integer trackedFaceCount = null;
                    // 记录切换时的人脸序号
                    if (faceHelper != null) {
                        trackedFaceCount = faceHelper.getTrackedFaceCount();
                        faceHelper.release();
                    }
                    faceHelper = new FaceHelper.Builder()
                            .ftEngine(ftEngine)
                            .frEngine(frEngine)
                            .flEngine(flEngine)
                            .frQueueSize(MAX_DETECT_NUM)
                            .flQueueSize(MAX_DETECT_NUM)
                            .previewSize(previewSize)
                            .faceListener(faceListener)
                            .trackedFaceCount(trackedFaceCount == null ? ConfigUtil.getTrackedFaceCount(context) : trackedFaceCount)
                            .build();
                }
            }


            @Override
            public void onPreview(final byte[] nv21, Camera camera) {

                List<FacePreviewInfo> facePreviewInfoList = faceHelper.onPreviewFrame(nv21);

                // 保存人脸数据
                mFaceData = nv21;
                mFacePreviewInfoList = facePreviewInfoList;

                clearLeftFace(facePreviewInfoList);

                if (facePreviewInfoList != null && facePreviewInfoList.size() > 0 && previewSize != null) {
                    detect_fail_num = 0;
                    for (int i = 0; i < facePreviewInfoList.size(); i++) {
                        Integer status = requestFeatureStatusMap.get(facePreviewInfoList.get(i).getTrackId());
                        /**
                         * 在活体检测开启，在人脸识别状态不为成功或人脸活体状态不为处理中（ANALYZING）且不为处理完成（ALIVE、NOT_ALIVE）时重新进行活体检测
                         */
                        if (Constants.livenessDetect && (status == null || status != RequestFeatureStatus.SUCCEED)) {
                            Integer liveness = livenessMap.get(facePreviewInfoList.get(i).getTrackId());
                            if (liveness == null
                                    || (liveness != LivenessInfo.ALIVE && liveness != LivenessInfo.NOT_ALIVE && liveness != RequestLivenessStatus.ANALYZING)) {
                                livenessMap.put(facePreviewInfoList.get(i).getTrackId(), RequestLivenessStatus.ANALYZING);
                                faceHelper.requestFaceLiveness(nv21, facePreviewInfoList.get(i).getFaceInfo(), previewSize.width, previewSize.height, FaceEngine.CP_PAF_NV21, facePreviewInfoList.get(i).getTrackId(), LivenessType.RGB);

                            }
                        }
                        /**
                         * 对于每个人脸，若状态为空或者为失败，则请求特征提取（可根据需要添加其他判断以限制特征提取次数），
                         * 特征提取回传的人脸特征结果在{@link FaceListener#onFaceFeatureInfoGet(FaceFeature, Integer, Integer)}中回传
                         */
                        if (status == null
                                || status == RequestFeatureStatus.TO_RETRY) {
                            requestFeatureStatusMap.put(facePreviewInfoList.get(i).getTrackId(), RequestFeatureStatus.SEARCHING);
                            faceHelper.requestFaceFeature(nv21, facePreviewInfoList.get(i).getFaceInfo(), previewSize.width, previewSize.height, FaceEngine.CP_PAF_NV21, facePreviewInfoList.get(i).getTrackId());
//                            Log.i(TAG, "onPreview: fr start = " + System.currentTimeMillis() + " trackId = " + facePreviewInfoList.get(i).getTrackedFaceCount());
                        }
                    }
                }else {

                    if(detect_fail_num > MAX_DETECT_FAIL_NUM){

                        if(mSpotCallback != null){
                            mSpotCallback.detectFail();
                        }
                        stopRecognize();

                    }else {
                        detect_fail_num += 1;
                    }
                }
            }

            @Override
            public void onCameraClosed() {
                Log.i(TAG, "onCameraClosed: ");
            }

            @Override
            public void onCameraError(Exception e) {
                Log.i(TAG, "onCameraError: " + e.getMessage());
            }

            @Override
            public void onCameraConfigurationChanged(int cameraID, int displayOrientation) {

                Log.i(TAG, "onCameraConfigurationChanged: " + cameraID + "  " + displayOrientation);
            }
        };

        cameraHelper = new CameraHelper.Builder()
                .previewViewSize(new Point(previewView.getMeasuredWidth(), previewView.getMeasuredHeight()))
                .rotation(0)
                .specificCameraId(RGB_CAMERA_ID)
                .isMirror(false)
                .previewOn(previewView)
                .cameraListener(cameraListener)
                .build();
        cameraHelper.init();
        cameraHelper.start();
    }

    private void searchFace(final FaceFeature frFace, final Integer requestId) {
        Observable
                .create(new ObservableOnSubscribe<CompareResult>() {
                    @Override
                    public void subscribe(ObservableEmitter<CompareResult> emitter) {
//                        Log.i(TAG, "subscribe: fr search start = " + System.currentTimeMillis() + " trackId = " + requestId);
                        CompareResult compareResult = FaceTools.getInstance().getTopOfFaceLib(frFace);
//                        Log.i(TAG, "subscribe: fr search end = " + System.currentTimeMillis() + " trackId = " + requestId);
                        emitter.onNext(compareResult);

                    }
                })
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<CompareResult>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(CompareResult compareResult) {
                        if (compareResult == null || compareResult.getUserName() == null) {
                            requestFeatureStatusMap.put(requestId, RequestFeatureStatus.FAILED);
                            faceHelper.setName(requestId, "VISITOR " + requestId);
                            return;
                        }

//                        Log.i(TAG, "onNext: fr search get result  = " + System.currentTimeMillis() + " trackId = " + requestId + "  similar = " + compareResult.getSimilar());
                        if (compareResult.getSimilar() > SIMILAR_THRESHOLD) {
                            boolean isAdded = false;
                            if (compareResultList == null) {
                                requestFeatureStatusMap.put(requestId, RequestFeatureStatus.FAILED);
                                faceHelper.setName(requestId, "VISITOR " + requestId);
                                return;
                            }
                            for (CompareResult compareResult1 : compareResultList) {
                                if (compareResult1.getTrackId() == requestId) {
                                    isAdded = true;
                                    break;
                                }
                            }
                            if (!isAdded) {
                                //对于多人脸搜索，假如最大显示数量为 MAX_DETECT_NUM 且有新的人脸进入，则以队列的形式移除
                                if (compareResultList.size() >= MAX_DETECT_NUM) {
                                    compareResultList.remove(0);
                                }
                                //添加显示人员时，保存其trackId
                                compareResult.setTrackId(requestId);
                                compareResultList.add(compareResult);
                            }
                            requestFeatureStatusMap.put(requestId, RequestFeatureStatus.SUCCEED);
                            faceHelper.setName(requestId, "recognize_success_notice:" + compareResult.getUserName());


                            //检测到的人脸
                            if (mSpotCallback != null) {
                                mSpotCallback.spotFaceId(compareResult.getUserName());
                            }
                            stopRecognize();

                        } else {
                            faceHelper.setName(requestId, "recognize_failed_notice:" + "NOT_REGISTERED");
//                            retryRecognizeDelayed(requestId);


                            //检测到人脸，但没录入
                            if (mSpotCallback != null) {
                                mSpotCallback.unRegister(getHeadBmp());
                            }
                            stopRecognize();

                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        faceHelper.setName(requestId, "recognize_failed_notice:" + "NOT_REGISTERED");
//                        retryRecognizeDelayed(requestId);


                        //检测到人脸，但没录入
                        if (mSpotCallback != null) {
                            mSpotCallback.unRegister(getHeadBmp());
                        }
                        stopRecognize();

                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    /**
     * 销毁引擎，faceHelper中可能会有特征提取耗时操作仍在执行，加锁防止crash
     */
    private void unInitEngine() {
        if (ftInitCode == ErrorInfo.MOK && ftEngine != null) {
            synchronized (ftEngine) {
                int ftUnInitCode = ftEngine.unInit();
                Log.i(TAG, "unInitEngine: " + ftUnInitCode);
            }
        }
        if (frInitCode == ErrorInfo.MOK && frEngine != null) {
            synchronized (frEngine) {
                int frUnInitCode = frEngine.unInit();
                Log.i(TAG, "unInitEngine: " + frUnInitCode);
            }
        }
        if (flInitCode == ErrorInfo.MOK && flEngine != null) {
            synchronized (flEngine) {
                int flUnInitCode = flEngine.unInit();
                Log.i(TAG, "unInitEngine: " + flUnInitCode);
            }
        }
    }

    /**
     * 删除已经离开的人脸
     */
    private void clearLeftFace(List<FacePreviewInfo> facePreviewInfoList) {
        if (compareResultList != null) {
            for (int i = compareResultList.size() - 1; i >= 0; i--) {
                if (!requestFeatureStatusMap.containsKey(compareResultList.get(i).getTrackId())) {
                    compareResultList.remove(i);
                }
            }
        }
        if (facePreviewInfoList == null || facePreviewInfoList.size() == 0) {
            requestFeatureStatusMap.clear();
            livenessMap.clear();
            livenessErrorRetryMap.clear();
            extractErrorRetryMap.clear();
            if (getFeatureDelayedDisposables != null) {
                getFeatureDelayedDisposables.clear();
            }
            return;
        }
        Enumeration<Integer> keys = requestFeatureStatusMap.keys();
        while (keys.hasMoreElements()) {
            int key = keys.nextElement();
            boolean contained = false;
            for (FacePreviewInfo facePreviewInfo : facePreviewInfoList) {
                if (facePreviewInfo.getTrackId() == key) {
                    contained = true;
                    break;
                }
            }
            if (!contained) {
                requestFeatureStatusMap.remove(key);
                livenessMap.remove(key);
                livenessErrorRetryMap.remove(key);
                extractErrorRetryMap.remove(key);
            }
        }


    }

    /**
     * 获取人脸头像
     * */
    private Bitmap getHeadBmp(){
        // 创建一个头像的Bitmap，存放旋转结果图
        FaceInfo faceInfo =  mFacePreviewInfoList.get(0).getFaceInfo();
        if(faceInfo == null || mFaceData.length == 0){
            return null;
        }
        int width = previewSize.width;
        int height = previewSize.height;
        // 为了美观，扩大rect截取注册图
        Rect cropRect = ImageUtil.getBestRect(width, height, faceInfo.getRect());
        if (cropRect == null) {
            Log.e(TAG, "registerNv21: cropRect is null!");
            return null;
        }

        cropRect.left &= ~3;
        cropRect.top &= ~3;
        cropRect.right &= ~3;
        cropRect.bottom &= ~3;

        Bitmap headBmp = ImageUtil.getHeadImage(mFaceData.clone(), width, height, faceInfo.getOrient(), cropRect, ArcSoftImageFormat.NV21);
        return headBmp;
    }


    /**
     * 将map中key对应的value增1回传
     */
    public int increaseAndGetValue(Map<Integer, Integer> countMap, int key) {
        if (countMap == null) {
            return 0;
        }
        Integer value = countMap.get(key);
        if (value == null) {
            value = 0;
        }
        countMap.put(key, ++value);
        return value;
    }

    /**
     * 注册
     */
    private void registerFace(final byte[] nv21, final List<FacePreviewInfo> facePreviewInfoList, IRegisterResult registerResult) {

        String faceId = null;

        if (facePreviewInfoList != null && facePreviewInfoList.size() > 0) {

            faceId = FaceTools.getInstance().registerNv21(context, nv21.clone(), previewSize.width, previewSize.height,
                    facePreviewInfoList.get(0).getFaceInfo());
        }

        if (registerResult != null){
            registerResult.completed(faceId);
        }
    }

    /**
     * 延迟 FAIL_RETRY_INTERVAL 重新进行活体检测
     */
    private void retryLivenessDetectDelayed(final Integer requestId) {
        Observable.timer(FAIL_RETRY_INTERVAL, TimeUnit.MILLISECONDS)
                .subscribe(new Observer<Long>() {
                    Disposable disposable;

                    @Override
                    public void onSubscribe(Disposable d) {
                        disposable = d;
                        delayFaceTaskCompositeDisposable.add(disposable);
                    }

                    @Override
                    public void onNext(Long aLong) {

                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                    }

                    @Override
                    public void onComplete() {
                        // 将该人脸状态置为UNKNOWN，帧回调处理时会重新进行活体检测
                        if (Constants.livenessDetect) {
                            faceHelper.setName(requestId, Integer.toString(requestId));
                        }
                        livenessMap.put(requestId, LivenessInfo.UNKNOWN);
                        delayFaceTaskCompositeDisposable.remove(disposable);
                    }
                });
    }

    /**
     * 延迟 FAIL_RETRY_INTERVAL 重新进行人脸识别
     */
    private void retryRecognizeDelayed(final Integer requestId) {
        requestFeatureStatusMap.put(requestId, RequestFeatureStatus.FAILED);
        Observable.timer(FAIL_RETRY_INTERVAL, TimeUnit.MILLISECONDS)
                .subscribe(new Observer<Long>() {
                    Disposable disposable;

                    @Override
                    public void onSubscribe(Disposable d) {
                        disposable = d;
                        delayFaceTaskCompositeDisposable.add(disposable);
                    }

                    @Override
                    public void onNext(Long aLong) {

                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                    }

                    @Override
                    public void onComplete() {
                        // 将该人脸特征提取状态置为FAILED，帧回调处理时会重新进行活体检测
                        faceHelper.setName(requestId, Integer.toString(requestId));
                        requestFeatureStatusMap.put(requestId, RequestFeatureStatus.TO_RETRY);
                        delayFaceTaskCompositeDisposable.remove(disposable);
                    }
                });
    }


    /**
     * 销毁操作
     */
    private void stopRecognize() {


        if (cameraHelper != null) {
            cameraHelper.release();
            cameraHelper = null;
        }

        unInitEngine();
        if (faceHelper != null) {
            ConfigUtil.setTrackedFaceCount(context, faceHelper.getTrackedFaceCount());
            faceHelper.release();
            faceHelper = null;
        }
        if (getFeatureDelayedDisposables != null) {
            getFeatureDelayedDisposables.clear();
        }
        if (delayFaceTaskCompositeDisposable != null) {
            delayFaceTaskCompositeDisposable.clear();
        }

        FaceTools.getInstance().unInit();

        mWindowManager.removeView(mView);


    }

    private void showToast(String s) {
        Toast.makeText(context, s, Toast.LENGTH_SHORT).show();
    }
}
