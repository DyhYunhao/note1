package com.dyh.tbsdemo;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentActivity;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

public class MainActivity extends AppCompatActivity {
    private String url = "http://www.baidu.com";
    private WebView webView;
    private ProgressBar progressBar;

    @SuppressLint("SetJavaScriptEnabled")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        webView = findViewById(R.id.wv_demo1);
        progressBar = (ProgressBar) findViewById(R.id.progressbar);//进度条
        WebSettings webSettings = webView.getSettings();
//        webSettings.setJavaScriptEnabled(true);

        //是否支持缩放,默认是true
        webSettings.setSupportZoom(true);
        //是否需要用户手势来播放Media,默认true
        webSettings.setMediaPlaybackRequiresUserGesture(true);
        //是否使用WebView内置的缩放组件，由浮动在窗口上的缩放控制和手势缩放控制组成，默认false
        webSettings.setBuiltInZoomControls(false);
        //是否显示窗口悬浮的缩放控制，默认true
        webSettings.setDisplayZoomControls(true);
        //是否允许访问webView内部文件，默认true
        webSettings.setAllowFileAccess(true);
        //是否允许获取WebView的内容URL 可以让WebView访问ContentPrvider存储的内容。 默认true
        webSettings.setAllowFileAccess(true);
        //是否启动概述模式浏览界面，当页面宽度超过WebView显示宽度时，缩小页面适应WebView，默认false
        webSettings.setLoadWithOverviewMode(false);
        //是否保存表单数据，默认false
        webSettings.setSaveFormData(false);
        //设置页面文字缩放百分比,默认100%
        webSettings.setTextZoom(100);
        //是否支持ViewPort的meta tag属性，如果页面有ViewPort meta tag 指定的宽度，则使用meta tag指定的值，否则默认使用宽屏的视图窗口
        webSettings.setUseWideViewPort(true);
        //是否支持多窗口，如果设置为true,WebChromeClient#onCreateWindow方法必须被主程序实现，默认false
        webSettings.setSupportMultipleWindows(false);
        //指定webView的页面布局显示形式，调用该方法会引起页面重绘。默认LayoutAlgorithm#NARROW_COLUMNS
        webSettings.setLayoutAlgorithm(WebSettings.LayoutAlgorithm.NARROW_COLUMNS);
        //设置是否加载图片资源，注意：方法控制所有的资源图片显示，包括嵌入的本地图片资源。
        //使用方法setBlockNetworkImage则只限制网络资源图片的显示。值设置为true后，
        // webview会自动加载网络图片。默认true
        webSettings.setLoadsImagesAutomatically(true);
        //是否加载网络图片资源。注意如果getLoadsImagesAutomatically返回false，则该方法没有效果
        //如果使用setLoadsImagesAutomatically设置为false，该方法设置为false，也不会显示网络图片。
        //当值从true改为false时。WebView会自动加载网络图片
        webSettings.setBlockNetworkImage(false);
        //设置是否加载网络资源。注意如果值从true切换为false后，WebView不会自动加载，除非调用WebView#reload().
        // 如果没有android.Manifest.permission#INTERNET权限，值设为false，则会抛出java.lang.SecurityException异常。
        //默认值：有android.Manifest.permission#INTERNET权限时为false，其他为true。
        webSettings.setBlockNetworkLoads(false);
        //设置是否允许执行JS
        webSettings.setJavaScriptEnabled(true);
        //是否允许Js访问任何来源的内容。包括访问file scheme的URLs。考虑到安全性限制Js访问范围默认禁用
        //该方法只影响file scheme类型的资源，其他类型资源如图片类型的不会受到影响
        //ICE_CREAM_SANDWICH_MR1版本以及以下默认为true，JELLY_BEAN版本以后的版本默认禁止
        webSettings.setAllowFileAccessFromFileURLs(false);
        //是否允许Cache，默认false。考虑需要存储缓存，应该为缓存指定存储路径setAppCachePath
        webSettings.setAppCacheEnabled(false);

        webView.loadUrl(url);
        webView.setWebViewClient(new WebViewClient() {

            @Override
            public void onPageFinished(WebView view, String url) {
                progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                progressBar.setVisibility(View.VISIBLE);
            }

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }
        });
    }
}