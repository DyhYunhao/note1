package com.dyh.kfirst.type

/**
 * describe:kotlin 数据类型
 * create by daiyh on 2021-7-26
 */

fun main() {
    //1.如何声明一个基本数据类型的变量，有哪些方式
    //基本数据类型的整型默认类型均为 Int,如果超过了Int的取值范围，自动上升为Long
    val number = 100 //根据赋值的100自动推断变数据类型为Int
    val bigNumber = 8000000000 //超过了Int的取值范围，升级为Long
    val longNumber = 20L //指定数据类型为Long
    val byteNumber: Byte = 1 //指定数据类型为Byte,变量后面加：和数据类型
    val doubleNumber = 3.1415926535898 //默认Double类型
    val floatNumber = 3.1415926535898f//后面加f或F，表示是Float类型的浮点数

    println("floatNumber: $floatNumber") //float的小数点后最多保留6位，四舍五入因此值为3.1415927
    println("doubleNumber: $doubleNumber")//float的小数点后最多保留16位

    val char: Char = '0' //字符类型，单引号定义
    val boolean: Boolean = true //布尔类型，true/false
    val string: String = "123123123" //字符串类型
}