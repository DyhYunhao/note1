package com.dyh.kfirst.coroutine

import android.util.Log
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import kotlin.system.measureNanoTime
import kotlin.system.measureTimeMillis

/**
 * describe:
 * create by daiyh on 2021-8-17
 */
object Demo1 {

    fun testLaunch() {
        val time: Long = measureNanoTime {
            GlobalScope.launch {
                Thread.sleep(1000)
                Log.d("tk1", "testLaunch: launch1中，${Thread.currentThread()}我很好")
            }

            GlobalScope.launch {
                Thread.sleep(1000)
                Log.d("tk1", "testLaunch: launch2中，${Thread.currentThread()}嘿嘿嘿")
            }

            Log.d("tk1", "你好吗？ ${Thread.currentThread()}")

            Thread.sleep(22000)//这里需要等待，否则代码函数生命周期结束就销毁了函数栈，导致两个协程没有执行

        }

        Log.d("tk1", "函数总耗时: $time")
    }

    fun testBlock() {
        val time = measureTimeMillis {
            runBlocking {
                GlobalScope.launch {
                    Thread.sleep(1000)
                    Log.d("tk1", "launch1: ${Thread.currentThread()}")
                }

                GlobalScope.launch {
                    Thread.sleep(1000)
                    Log.d("tk1", "launch2: ${Thread.currentThread()}")
                }

            }

            Log.d("tk1", "testRunBlocking ${Thread.currentThread()} hello!!")
        }

        Log.d("tk1", "函数总耗时: $time")
    }

}