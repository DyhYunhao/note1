package com.dyh.kfirst

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.dyh.kfirst.coroutine.Demo1
import com.dyh.kfirst.http.OkHttpUtils

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

//        OkHttpUtils.instance.get("http://www.baidu.com")

//        OkHttpUtils.instance.getAsync("http://www.baidu.com")

//        Demo1.testLaunch()
        Demo1.testBlock()
    }
}