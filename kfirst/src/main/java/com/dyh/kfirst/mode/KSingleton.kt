package com.dyh.kfirst.mode

import android.app.Dialog
import androidx.annotation.ColorRes
import okhttp3.internal.wait
import kotlin.properties.Delegates

/**
 * describe:
 * create by daiyh on 2021-9-3
 */
//懒汉式单例模式，饿汉式单例直接使用object就行了
class KSingleton private constructor() {

    companion object {
        val instace: KSingleton by lazy { KSingleton() } //通过lazy的延迟加载实现
    }
}

/**
 * 工厂方法模式
 * 工厂方法把创建对象的过程抽象为接口，由工厂的子类决定对象的创建
 */
interface Product {
    val name: String
}
class ProductA(override val name: String = "ProductA"): Product
class ProductB(override val name: String = "ProductB"): Product

/**
 * 建造者模式
 * 建造者模式是为了构建复杂对象的，一般情况下，Kotlin 中使用标准的apply函数就可以
 */
//val dialog = Dialog(context).apply {
//    setTitle("DialogA")
//    setCancelable(true)
//    setCanceledOnTouchOutside(true)
//    setContentView(contentView)
//}

/**
 * 原型模式
 * 原型模式是以一个对象为原型，创建出一个新的对象，
 * 在 Kotlin 下很容易实现，因为使用 data class 时，会自动获得equals、hashCode、toString和copy方法，
 * 而copy方法可以克隆整个对象并且允许修改新对象某些属性。
 */
data class EMail(var re: String, var sub: String?, var message: String?)
val mail = EMail("ab@c.com", "hello", "do..")
val copy = mail.copy(re = "de@f.com")

/**
 * 适配器模式
 * 适配器模式是把一个不兼容的接口转化为另一个类可以使用的接口
 */
interface Target {
    fun request()
}

interface Adaptee {
    fun ask()
}

class Adapter(val wrapper: Adaptee): Target {
    override fun request() {
        wrapper.ask()
    }
}

/**
 * 桥接模式
 * 桥接模式可以分离某个类存在两个独立变化的纬度，把多层继承结构改为两个独立的继承结构，在两个抽象层中有一个抽象关联
 */
interface Color {
    fun coloring()
}
class RedColor: Color {
    override fun coloring() {
        TODO("Not yet implemented")
    }
}
class BlueColor: Color {
    override fun coloring() {
        TODO("Not yet implemented")
    }
}
interface Pen {
    val colorImpl: Color   //这个就是桥
    fun write()
}
class BigPen(override val colorImpl: Color) : Pen {
    override fun write() {
        TODO("Not yet implemented")
    }
}
class SmallPen(override val colorImpl: Color) : Pen {
    override fun write() {
        TODO("Not yet implemented")
    }
}

/**
 *组合模式
 * 组合模式是对树形结构的处理，让调用者忽视单个对象和组合结构的差异，
 * 通常会新增包含叶子节点和容器节点接口的抽象构件 Component
 */
interface AbstractFile {    // Component
    var childCount: Int
    fun getChild(i: Int): AbstractFile
    fun size(): Long
}
class File(val size: Long, override var childCount: Int = 0) : AbstractFile {
    override fun getChild(i: Int): AbstractFile {
        throw RuntimeException("You shouldn't call the method in File")
    }
    override fun size() = size
}
class Folder(override var childCount: Int) : AbstractFile {
    override fun getChild(i: Int): AbstractFile {
        throw RuntimeException("You shouldn't call the method in File")
    }
    override fun size(): Long {
        return (0..childCount)
            .map { getChild(it).size() }
            .sum()
    }
}

/**
 *装饰模式
 * 装饰模式可以给一个对象添加额外的行为，在 Kotlin 中可以通过扩展函数简单的实现
 */
class Text(val text: String) {
    fun draw() = print(text)
}
fun Text.underline(decorated: Text.() -> Unit) {
    print("_")
    this.decorated()
    print("_")
}
// usage
//Text("Hello").run {
//    underline {
//        draw()
//    }
//}

/**
 * 外观模式
 * 外观模式是为一个复杂的子系统提供一个简化的统一接口
 */


/**
 * 观察者模式
 * 观察者模式是一个对象状态发生变化后，可以立即通知已订阅的另一个对象
 */
interface TextChangedListener {
    fun onTextChanged(newText: String)
}

class TextView {
    var listener: TextChangedListener? = null

    var text: String by Delegates.observable("") { prop, old, new ->
        listener?.onTextChanged(new)
    }
}

