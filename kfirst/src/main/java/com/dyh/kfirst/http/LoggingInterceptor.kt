package com.dyh.kfirst.http

import android.util.Log
import okhttp3.Interceptor
import okhttp3.Response
import okhttp3.ResponseBody
import okio.Buffer


/**
 * describe:
 * create by daiyh on 2021-8-15
 */
class LoggingInterceptor : Interceptor {

    val TAG = "http"

    override fun intercept(chain: Interceptor.Chain): Response {
        val timeStart = System.nanoTime()
        val request = chain.request()
        val response = chain.proceed(request)
        val buffer = Buffer()
        request.body?.writeTo(buffer)
        val requestStr = buffer.readUtf8()

        Log.d(TAG, String.format("sending request %s with params %s", request.url, requestStr))
        val bussinessData = response.body?.toString() ?: "response body is null"
        val mediaType = response.body?.contentType()
        val newBody = ResponseBody.create(mediaType, bussinessData)
        val newResponse = response.newBuilder().body(newBody).build()
        val timeEnd = System.nanoTime()
        Log.d(
            TAG,
            String.format(
                "receive response for %s in %.1fms >>> %s",
                request.url,
                (timeEnd - timeStart) / 1e6,
                bussinessData
            )
        )
        return newResponse
    }
}