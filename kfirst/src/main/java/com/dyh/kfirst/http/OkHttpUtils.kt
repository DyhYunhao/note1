package com.dyh.kfirst.http

import android.util.Log
import okhttp3.*
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.internal.notify
import okhttp3.logging.HttpLoggingInterceptor
import org.json.JSONObject
import java.io.IOException
import java.util.concurrent.TimeUnit

/**
 * describe:
 * create by daiyh on 2021-8-15
 */
class OkHttpUtils private constructor() {

    companion object {

        @JvmField
        val TAG = "http"

        //通过@JvmStatic注解，使得在Java中调用instance直接是像调用静态函数一样，
        //类似KLazilyDCLSingleton.getInstance(),如果不加注解，在Java中必须这样调用: KLazilyDCLSingleton.Companion.getInstance().
        @JvmStatic
        val instance: OkHttpUtils by lazy(mode = LazyThreadSafetyMode.SYNCHRONIZED) {
            OkHttpUtils()
        }
    }

    /**
     * DCL虽然在一定程度上能解决资源消耗、多余synchronized同步、线程安全等问题，
     * 但是某些情况下还会存在DCL失效问题，尽管在JDK1.5之后通过具体化volatile原语来解决DCL失效问题，
     * 但是它始终并不是优雅一种解决方式，在多线程环境下一般不推荐DCL的单例模式。所以引出静态内部类单例实现
     *  companion object {
     *      @JvmStatic
     *      fun getInstance(): OkHttpUtils {//全局访问点
     *          return SingletonHolder.mInstance
     *      }
     *  }
     *  private object SingletonHolder {
     *      val mInstance = OkHttpUtils()
     *  }
     */

    private val client: OkHttpClient
    init {
        val httpLoggingInterceptor = HttpLoggingInterceptor()
        httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY)
        client = OkHttpClient.Builder()  //builder构造者设计模式
            .connectTimeout(10, TimeUnit.SECONDS) //连接超时时间
            .readTimeout(10, TimeUnit.SECONDS) //读取超时时间
            .writeTimeout(10, TimeUnit.SECONDS) //写超时时间
            .addInterceptor(httpLoggingInterceptor) //拦截器
            .build()
    }

    //同步GET请求
    fun get(url: String) {

        Thread {
            val request = Request.Builder().url(url).build()
            val call = client.newCall(request)
            val response = call.execute()
            val body = response.body?.toString()
            Log.d(TAG, "get body is: ${body.toString()}")
        }.start()

    }

    //异步GET请求
    fun getAsync(url: String) {
        val request = Request.Builder().url(url).build()
        val call = client.newCall(request)
        val response = call.enqueue(object : Callback {
            override fun onFailure(call: Call, e: IOException) {
                Log.d(TAG, "get body is: ${e.message}")
            }

            override fun onResponse(call: Call, response: Response) {
                val body = response.body?.toString()
                Log.d(TAG, "get body is: ${body.toString()}")
            }

        })
    }

    //同步post请求
    fun post(url: String) {

        val param = FormBody.Builder().add("key", "value").build()
        val request = Request.Builder().url(url).post(param).build()
        val call = client.newCall(request)
        Thread{
            val response = call.execute()
            Log.d(TAG, "post: response is ${response.body?.string()}")
        }.start()
    }

    //异步post请求
    fun postAsync(url: String) {

        val param = FormBody.Builder().add("key", "value").build()
        val request = Request.Builder().url(url).post(param).build()
        val call = client.newCall(request)
        val response = call.enqueue(object :Callback{
            override fun onFailure(call: Call, e: IOException) {
                Log.d(TAG, "post: ${e.message}")
            }

            override fun onResponse(call: Call, response: Response) {
                Log.d(TAG, "post: response is ${response.body?.string()}")
            }

        })

    }

    //异步post请求多表单上传
    fun postAsyncMultipart(url :String){
        val param = MultipartBody.Builder().addFormDataPart("key", "value").build()
        val request = Request.Builder().url(url).post(param).build()
        val call = client.newCall(request)
        val response = call.enqueue(object :Callback{
            override fun onFailure(call: Call, e: IOException) {
                Log.d(TAG, "post: ${e.message}")
            }

            override fun onResponse(call: Call, response: Response) {
                Log.d(TAG, "post: response is ${response.body?.string()}")
            }

        })
    }

    //异步post请求提交字符串
    fun postAsyncString(url :String) {
        val jsonObj = JSONObject()
        jsonObj.put("key1", "value1")
        jsonObj.put("key2", "value2")
        val param = RequestBody.create("application/json;charset=utf-8".toMediaType(), jsonObj.toString())

        val request = Request.Builder().url(url).post(param).build()
        val call = client.newCall(request)
        val response = call.enqueue(object :Callback{
            override fun onFailure(call: Call, e: IOException) {
                Log.d(TAG, "post: ${e.message}")
            }

            override fun onResponse(call: Call, response: Response) {
                Log.d(TAG, "post: response is ${response.body?.string()}")
            }

        })
    }

}