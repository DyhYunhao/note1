package com.dyh.sdk1;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

/**
 * describe: sdk的上下文
 * create by daiyh on 2021-7-8
 */
public class SdkDemo extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initMineSdk("远泊谁与同", "来从古木中");
    }

    private String initMineSdk(String code, String key) {
        return code + key;
    }
}
