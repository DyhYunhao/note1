package com.dyh.myview.utils;

import android.content.Context;

/**
 * describe: 单位换算工具类,没有配置信息使用静态类就好，如果有多个配置信息需要使用单例
 * create by daiyh on 2021-7-7
 */
public class UnitUtil {

    /**
     * dp 转 px
     * @param context 使用类的上下文
     * @param dp 传入的dp值
     * @return px 的值
     */
    public static int dp2px(Context context, float dp) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int) (dp * scale + 0.5f);
    }

}
