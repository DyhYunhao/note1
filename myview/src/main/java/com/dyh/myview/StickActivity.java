package com.dyh.myview;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.dyh.myview.view.CustomLoadingView;

public class StickActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stick);
    }

    @Override
    protected void onResume() {
        ((CustomLoadingView)findViewById(R.id.loadingView)).start();
        super.onResume();
    }
}