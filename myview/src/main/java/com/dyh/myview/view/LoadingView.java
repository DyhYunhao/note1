package com.dyh.myview.view;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.LinearInterpolator;

import androidx.annotation.Nullable;

import com.dyh.myview.utils.UnitUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * describe: 加载球动画
 * create by daiyh on 2021-7-7
 */
public class LoadingView extends View {
    private Paint mPaint;
    //线条长度
    private int mLineLength = UnitUtil.dp2px(getContext(), 40);
    //线条当前长度
    private float mCurrentLineLength;
    //当前的width和height
    private int mWidth = 0;
    private int mHeight = 0;
    //起始角度
    private int mAngle = 60;
    //绘制圆的半径
    private int mCircleRadius;
    //绘制圆的距离
    private float mCircleY;
    //线条颜色
    private int[] mColors = new int[]{0xB07ECBDA, 0xB0E6A92C, 0xB0D6014D, 0xB05ABA94};
    //当前步骤
    private int mCurrentStep = 1;
    //当前状态
    private String mCurrentStatus = "normal";
    //执行动画集合
    private List<Animator> mAnimatorList;

    public LoadingView(Context context) {
        super(context);
    }

    public LoadingView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public LoadingView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView();
        initData();
    }

    private void initView() {
        if (mPaint == null) {
            mPaint = new Paint();
            mPaint.setAntiAlias(true);
            mPaint.setStrokeCap(Paint.Cap.ROUND);
            mPaint.setStrokeWidth(48f);
        }
    }

    private void initData() {
        mCircleY = 0;
        mCurrentStep = 1;
        mCurrentStatus = "normal";
        mAnimatorList = new ArrayList<>();
        mAngle = 60;
        mLineLength = UnitUtil.dp2px(getContext(), 50);
        mCurrentLineLength = mLineLength;
        mCircleRadius = mLineLength / 5;
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        mWidth = w;
        mHeight = h;
        super.onSizeChanged(w, h, oldw, oldh);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        float startX = (mWidth >> 1) - (mLineLength >> 1);
        float startY = (mHeight >> 1) - mLineLength;
        float endX = startX;
        float endY = (mHeight >> 1) + mLineLength;
        int step = mCurrentStep % 4;
        switch (step) {
            case 1:
                for (int i = 0; i < mColors.length; i++) {
                    mPaint.setColor(mColors[i]);
                    canvas.rotate(mAngle + 90 * i, mWidth >> 1, mHeight >> 1);
                    canvas.drawLine(startX, mHeight / 2 - mCurrentLineLength, endX, endY, mPaint);
                    canvas.rotate(-(mAngle + 90 * i), mWidth / 2, mHeight / 2);
                }
                break;
            case 2:
                for (int i = 0; i < mColors.length; i++) {
                    mPaint.setColor(mColors[i]);
                    canvas.rotate(mAngle + 90 * i, mWidth >> 1, mHeight >> 1);
                    canvas.drawCircle(endX, endY, mCircleRadius, mPaint);
                    canvas.rotate(-(mAngle + 90 * i), mWidth / 2, mHeight / 2);
                }
                break;
            case 3:
                for (int i = 0; i < mColors.length; i++) {
                    mPaint.setColor(mColors[i]);
                    canvas.rotate(mAngle + 90 * i, mWidth >> 1, mHeight >> 1);
                    canvas.drawCircle(startX, mHeight / 2 + mCircleY, mCircleRadius, mPaint);
                    canvas.rotate(-(mAngle + 90 * i), mWidth / 2, mHeight / 2);
                }
                break;
            case 4:
                for (int i = 0; i < mColors.length; i++) {
                    mPaint.setColor(mColors[i]);
                    canvas.rotate(mAngle + 90 * i, mWidth >> 1, mHeight >> 1);
                    canvas.drawLine(startX, endY, endX, mHeight / 2 + mCurrentLineLength, mPaint);
                    canvas.rotate(-(mAngle + 90 * i), mWidth / 2, mHeight / 2);
                }
                break;
        }
        super.onDraw(canvas);
    }

    private void cancelAnim() {
        if (!mAnimatorList.isEmpty()) {
            for (Animator animator : mAnimatorList) {
                if (animator.isRunning()) {
                    animator.cancel();
                }
            }
            mAnimatorList.clear();
        }
    }

    public void start() {
        if (mCurrentStatus.equals("normal")) {
            mCurrentStatus = "loading";
            cancelAnim();
            startAnimOne();
        }
    }

    public void stop() {
        if (mCurrentStatus.equals("loading")) {
            mCurrentStatus = "normal";
            cancelAnim();
            initData();
        }
    }

    public void clean() {
        cancelAnim();
    }

    public boolean isStart() {
        return mCurrentStatus.equals("loading");
    }

    private void startAnimOne() {
        ValueAnimator rotateAnim = ValueAnimator.ofInt(60, 420);
        rotateAnim.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                mAngle = (int) animation.getAnimatedValue();
            }
        });
        ValueAnimator lineHeightAnim = ValueAnimator.ofFloat(mLineLength, -mLineLength);
        lineHeightAnim.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                mCurrentLineLength = (float) animation.getAnimatedValue();
                invalidate();
            }
        });
        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.play(rotateAnim).with(lineHeightAnim);
        animatorSet.setInterpolator(new LinearInterpolator());
        animatorSet.setDuration(500);
        animatorSet.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                if (mCurrentStatus.equals("loading")) {
                    startAnimTwo();
                    mCurrentStep++;
                }
                super.onAnimationEnd(animation);
            }
        });
        if (mCurrentStatus.equals("loading")) {
            mAnimatorList.add(animatorSet);
            animatorSet.start();
        }
    }

    private void startAnimTwo() {
        ValueAnimator rotateAnim = ValueAnimator.ofInt(mAngle, mAngle + 180);
        rotateAnim.setInterpolator(new LinearInterpolator());
        rotateAnim.setDuration(500);
        rotateAnim.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                mAngle = (int) animation.getAnimatedValue();
                invalidate();
            }
        });
        rotateAnim.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                if (mCurrentStatus.equals("loading")) {
                    mCurrentStep++;
                    startAnimThree();
                }
                super.onAnimationEnd(animation);
            }
        });
        if (mCurrentStatus.equals("loading")) {
            mAnimatorList.add(rotateAnim);
            rotateAnim.start();
        }
    }

    private void startAnimThree() {
        ValueAnimator rotateAnim = ValueAnimator.ofInt(mAngle + 90, mAngle + 180);
        rotateAnim.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                mAngle = (int) animation.getAnimatedValue();
            }
        });
        ValueAnimator circleAnim = ValueAnimator.ofFloat(mLineLength, mLineLength / 4, mLineLength);
        circleAnim.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                mCircleY = (float) animation.getAnimatedValue();
                invalidate();
            }
        });
        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.play(rotateAnim).with(circleAnim);
        animatorSet.setDuration(1000);
        animatorSet.setInterpolator(new LinearInterpolator());
        animatorSet.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                if (mCurrentStatus.equals("loading")) {
                    mCurrentStep++;
                    startAnimFour();
                }
                super.onAnimationEnd(animation);
            }
        });
        if (mCurrentStatus.equals("loading")) {
            mAnimatorList.add(rotateAnim);
            rotateAnim.start();
        }
    }

    private void startAnimFour() {
        ValueAnimator lineAnim = ValueAnimator.ofFloat(mLineLength, -mLineLength);
        lineAnim.setDuration(500);
        lineAnim.setInterpolator(new LinearInterpolator());
        lineAnim.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                mCurrentLineLength = (float) animation.getAnimatedValue();
                invalidate();
            }
        });
        lineAnim.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                if (mCurrentStatus.equals("loading")) {
                    mCurrentStep++;
                    startAnimOne();
                }
                super.onAnimationEnd(animation);
            }
        });
        if (mCurrentStatus.equals("loading")) {
            mAnimatorList.add(lineAnim);
            lineAnim.start();
        }
    }
}
